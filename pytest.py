def process1():
    raise BaseException("Wrong user input for field X")  # Noncompliant

def process2():
    raise BaseException("Wrong configuration")  # Noncompliant

def process3(param):
    if not isinstance(param, int):
        raise Exception("param should be an integer")  # Noncompliant

def caller():
    try:
         process1()
         process2()
         process3()
    except BaseException as e:
        if e.args[0] == "Wrong user input for field X":
            # process error
            pass
        elif e.args[0] == "Wrong configuration":
            # process error
            pass
        else:
            # re-raise other exceptions
            raise